//NOTES: THIS IS TO JUST ADD : COEXMALL : SHOP NOW TO OUR HOMEPAGE FOR CUSTOMER ONLY
import { Row, Col } from 'react-bootstrap';
export default function Banner({ data }) {
    console.log(data);
    const { title, content, destination, label } = data;

    return (
        <Row>
            <Col>
                <h1>{title}</h1>
                <p>{content}</p>
            </Col>
        </Row>
    );
}
