//NOTES THIS IS ONLY USE FOR ADDING TITLE HEAD IN A PAGE IN GETTING ACTIVE PRODUCTS
//FOR TITLE PURPOSE ONLY

import { Row, Col, Container } from 'react-bootstrap';
import UserContext from '../UserContext';
import { useContext } from 'react';
import Banners from '../components/Banners';

export default function GetActive() {
    const { user } = useContext(UserContext);
    return (
        <>
            {user.isAdmin === true ? (
                <>
                    <Container fluid>
                        <Row className="mb-sm-5">
                            <Col>
                                <div
                                    style={{
                                        backgroundColor: '#E55C0D',
                                        color: '#FFA500',
                                        paddingTop: '2rem',
                                        paddingBottom: '2rem',
                                    }}
                                />
                            </Col>
                        </Row>
                    </Container>
                    <Container>
                        <Row>
                            <Col>
                                <h1>Available Products</h1>
                            </Col>
                        </Row>
                    </Container>
                </>
            ) : (
                <>
                    <Container fluid>
                        <Row className="mb-sm-5">
                            <Col>
                                <div
                                    style={{
                                        backgroundColor: '#E55C0D',
                                        color: '#FFA500',
                                        paddingTop: '2rem',
                                        paddingBottom: '2rem',
                                    }}
                                />
                            </Col>
                        </Row>
                    </Container>

                    <Container>
                        <Banners className="mt-sm-4" />
                        <Row>
                            <Col>
                                <h1>Available Products</h1>
                            </Col>
                        </Row>
                    </Container>
                </>
            )}
        </>
    );
}
