//NOTES: THIS IS ONLY FOR ADMIN PAGE
//THIS IS USE TO SHOW THE LIST OF ACTIVE PRODUCTS ONLY
//YOU CAN ALSO SEE THE DETAILS OF THE PRODUCT BUT YOU CAN'T EDIT

import { Button, Card, Col } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function ProductCard({ product }) {
    const { _id, name, description, price, image } = product;
    const navigate = useNavigate();
    function handleLogout() {
        Swal.fire({
            title: 'Login first!',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ok!',
        }).then((result) => {
            if (result.isConfirmed) {
                Swal.fire('Log in now!', 'This will give you access to exclusive deals,.');
                navigate('/login');
            }
        });
    }

    return (
        // </Card>
        <Col xs={12} md={3} className="mt-3 mb-3">
            <Card className="cardHighlight">
                <Card.Body>
                    <Card.Img src={image} className="img-fluid" />
                    <Card.Title>
                        <h5 className="product-names">{name}</h5>
                    </Card.Title>
                    <Card.Text>{description}</Card.Text>
                    <Card.Text className="price-color">
                        <p>
                            <span>&#8369;</span> {price}
                        </p>
                    </Card.Text>
                    <Button onClick={handleLogout}>Buy Now</Button>
                </Card.Body>
            </Card>
        </Col>
    );
}
