//NOTE: THIS IS FOR CUSTOMER'S EYE ONLY
//WHEN YOU CLICK THE SHOPNOW IN THE NAVBAR, IT WILL ROUTE HERE.
//THIS IS WHERE CX CAN ADD TO CART AND THEN WILL ROUTE TO CHECKOUT ONCE HITTING THE "ADD TO CART BUTTON"

import { Link } from 'react-router-dom';
import { useEffect, useState } from 'react';
import { Container, Row, Col, Form, Button, Card } from 'react-bootstrap';

export default function ProductCard({ product }) {
    const { _id, name, description, price, image } = product;

    return (
        <>
            <Col xs={12} md={3} className="mt-3 mb-3">
                <Card
                    className="cardHighlight"
                    style={{
                        boxShadow: '0px 4px 8px rgba(0, 0, 0, 0.1)',
                        transition: 'transform 0.2s ease-in-out',
                        transform: 'scale(1)',
                    }}
                    onMouseOver={(e) => {
                        e.currentTarget.style.transform = 'scale(1.06)';
                    }}
                    onMouseOut={(e) => {
                        e.currentTarget.style.transform = 'scale(1)';
                    }}
                >
                    <Card.Body as={Link} to={`/products/${_id}`} style={{ textDecoration: 'none' }}>
                        <Col>
                            <Card.Img src={image} className="img-fluid" />
                            <Card.Title>
                                <h5 className="product-names">{name}</h5>
                            </Card.Title>
                            <Card.Text>{description}</Card.Text>
                            <Card.Text className="price-color">
                                <p>
                                    <span>&#8369;</span> {price}
                                </p>
                            </Card.Text>
                        </Col>
                    </Card.Body>
                </Card>
            </Col>
        </>
    );
}
