//NOTES: THIS IS USE TO SHOW THE DETAILS OF THE PRODUCT, IF IT'S ADMIN, THE "BACK" BUTTON WILL SHOW; BUT IF IT'S CX, "CHECKOUT" (THAT WILL ROUTE FOR CX CHECKOUT) & "BACK" BUTTON WILL SHOW

//IMPORTS ============================================================
import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, Form } from 'react-bootstrap';
import { useParams, Link, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import React from 'react';
import AddToCartFE from '../pages/AddToCartFE';
import Swal from 'sweetalert2';
//EXPORTS ============================================================

export default function ProductView() {
    const { productId } = useParams();
    const { user } = useContext(UserContext);
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState(0);
    const [image, setImage] = useState('');
    const navigate = useNavigate();

    const handleCancel = () => {
        navigate(-1); // navigates to the previous page in the history stack
    };

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
            .then((res) => res.json())
            .then((data) => {
                setName(data.name);
                setDescription(data.description);
                setPrice(data.price);
                setImage(data.image);
            });
    }, [productId]);

    function Product({ name, price, onAddToCart }) {
        const [quantity, setQuantity] = useState(1);

        const handleQuantityChange = (event) => {
            setQuantity(parseInt(event.target.value));
        };

        const handleAddToCartClick = () => {
            onAddToCart(name, price, quantity);
        };

        return (
            <div>
                <h3>{name}</h3>
                <p>${price.toFixed(2)}</p>
                <Form.Group>
                    <Form.Label htmlFor="quantity">Quantity:</Form.Label>
                    <Form.Control
                        type="number"
                        id="quantity"
                        name="quantity"
                        min="1"
                        value={quantity}
                        onChange={handleQuantityChange}
                        className="my-3"
                    />
                </Form.Group>

                <Col md={12}>
                    <Button
                        onClick={handleAddToCartClick}
                        // onClick={handleCancel}
                        className="w-100"
                        variant="primary"
                        style={{
                            border: '1px solid #ccc',
                            padding: '20px',
                            boxShadow: '0px 0px 10px rgba(0, 0, 0, 0.2)',
                            backgroundColor: '#FEAD00  ',
                        }}
                    >
                        Add To Cart
                    </Button>
                </Col>
            </div>
        );
    }

    function Cart({ items, onRemoveFromCart }) {
        const getTotalPrice = () => {
            return items.reduce((total, item) => total + item.price * item.quantity, 0);
        };

        const handleRemoveFromCartClick = (item) => {
            onRemoveFromCart(item);
        };

        return (
            <div>
                <h3>Shopping Cart</h3>
                {items.map((item, index) => (
                    <div key={index}>
                        <p>
                            {item.name} ({item.quantity}) - ${item.price.toFixed(2)} each
                        </p>
                        <Button onClick={() => handleRemoveFromCartClick(item)} variant="danger">
                            Remove
                        </Button>
                    </div>
                ))}
                <p>Total: ${getTotalPrice().toFixed(2)}</p>
            </div>
        );
    }

    const [cartItems, setCartItems] = useState([]);

    const handleAddToCart = (name, price, quantity) => {
        const itemIndex = cartItems.findIndex((item) => item.name === name);
        if (itemIndex === -1) {
            // Item is not yet in the cart
            setCartItems([...cartItems, { name, price, quantity }]);
        } else {
            // Item is already in the cart
            const updatedCartItems = [...cartItems];
            updatedCartItems[itemIndex].quantity += quantity;
            setCartItems(updatedCartItems);
        }
    };

    const handleRemoveFromCart = (item) => {
        const updatedCartItems = cartItems.filter((cartItem) => cartItem !== item);
        setCartItems(updatedCartItems);
    };

    // ===========================
    const [quantity, setQuantity] = useState(1);

    const product = (productId) => {
        const userId = user.id;
        const productName = name;

        fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`,
            },
            body: JSON.stringify({
                productId: productId,
                userId: userId,
                productName: productName,
                quantity: quantity,
            }),
        })
            .then((res) => res.json())
            .then((data) => {
                console.log(data);

                if (data) {
                    Swal.fire({
                        title: 'Checked Out!',
                        icon: 'success',
                        text: 'Thank you for Purchasing.',
                    });
                } else {
                    Swal.fire({
                        title: 'Something went wrong',
                        icon: 'error',
                        text: 'Please try again.',
                    });
                }
            });
    };

    useEffect(() => {
        console.log(productId);

        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
            .then((res) => res.json())
            .then((data) => {
                console.log(data);

                setName(data.name);
                setDescription(data.description);
                setPrice(data.price);
            });
    }, [productId]);

    const handleIncrement = () => {
        setQuantity(quantity + 1);
    };

    const handleDecrement = () => {
        if (quantity > 1) {
            setQuantity(quantity - 1);
        }
    };
    return (
        <>
            <Container fluid>
                <Row className="mb-sm-5">
                    <Col>
                        <div
                            style={{
                                backgroundColor: '#E55C0D',
                                color: '#FFA500',
                                paddingTop: '2rem',
                                paddingBottom: '2rem',
                            }}
                        />
                    </Col>
                </Row>
            </Container>
            <Container
                className="mt-5 pt-md-5 pt-lg-5 rounded-4"
                style={{
                    border: '1px solid #ccc',
                    padding: '20px',
                    boxShadow: '0px 0px 10px rgba(0, 0, 0, 0.2)',
                }}
            >
                <Row>
                    <Col md={4}>
                        <Card.Img src={image} className="img-fluid" />
                    </Col>
                    <Col md={8} className="">
                        <Card className="border-0">
                            {user.isAdmin === true ? (
                                <>
                                    <Card.Body>
                                        <Card.Title>{name}</Card.Title>
                                        <Card.Subtitle>Description:</Card.Subtitle>
                                        <Card.Text>{description}</Card.Text>
                                        <Card.Subtitle>Price:</Card.Subtitle>
                                        <Card.Text>Php {price}</Card.Text>
                                    </Card.Body>
                                    <Button
                                        variant="primary"
                                        as={Link}
                                        to="/seeAllProduct"
                                        onClick={handleCancel}
                                        className="w-100"
                                    >
                                        Back
                                    </Button>
                                </>
                            ) : (
                                <>
                                    <Container fluid className="my-5">
                                        <Row>
                                            <Container fluid>
                                                <Row className="justify-content-center">
                                                    <Col md={6}>
                                                        <Product
                                                            name={name}
                                                            price={price}
                                                            onAddToCart={handleAddToCart}
                                                        />
                                                    </Col>
                                                    <Col md={6}>
                                                        <Cart
                                                            items={cartItems}
                                                            onRemoveFromCart={handleRemoveFromCart}
                                                        />
                                                    </Col>
                                                </Row>
                                            </Container>

                                            <Col md={6}>
                                                <Button
                                                    className="w-100"
                                                    variant="primary"
                                                    onClick={() => product(productId)}
                                                    style={{
                                                        border: '1px solid #ccc',
                                                        padding: '20px',
                                                        boxShadow:
                                                            '0px 0px 10px rgba(0, 0, 0, 0.2)',
                                                        backgroundColor: '#FE5100  ',
                                                    }}
                                                >
                                                    Checkout
                                                </Button>
                                            </Col>
                                        </Row>
                                    </Container>
                                </>
                            )}
                        </Card>
                    </Col>
                </Row>
            </Container>
        </>
    );
}
