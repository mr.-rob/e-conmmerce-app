import React, { useState } from 'react';

function Product({ name, price, onAddToCart }) {
    const [quantity, setQuantity] = useState(1);

    const handleQuantityChange = (event) => {
        setQuantity(parseInt(event.target.value));
    };

    const handleAddToCartClick = () => {
        onAddToCart(name, price, quantity);
    };

    return (
        <div>
            <h3>{name}</h3>
            <p>${price.toFixed(2)}</p>
            <label htmlFor="quantity">Quantity:</label>
            <input
                type="number"
                id="quantity"
                name="quantity"
                min="1"
                value={quantity}
                onChange={handleQuantityChange}
            />
            <button onClick={handleAddToCartClick}>Add to Cart</button>
        </div>
    );
}

function Cart({ items, onRemoveFromCart }) {
    const getTotalPrice = () => {
        return items.reduce((total, item) => total + item.price * item.quantity, 0);
    };

    const handleRemoveFromCartClick = (item) => {
        onRemoveFromCart(item);
    };

    return (
        <div>
            <h3>Shopping Cart</h3>
            {items.map((item, index) => (
                <div key={index}>
                    <p>
                        {item.name} ({item.quantity}) - ${item.price.toFixed(2)} each
                    </p>
                    <button onClick={() => handleRemoveFromCartClick(item)}>Remove</button>
                </div>
            ))}
            <p>Total: ${getTotalPrice().toFixed(2)}</p>
        </div>
    );
}

function Cart() {
    const [cartItems, setCartItems] = useState([]);

    const handleAddToCart = (name, price, quantity) => {
        const itemIndex = cartItems.findIndex((item) => item.name === name);
        if (itemIndex === -1) {
            // Item is not yet in the cart
            setCartItems([...cartItems, { name, price, quantity }]);
        } else {
            // Item is already in the cart
            const updatedCartItems = [...cartItems];
            updatedCartItems[itemIndex].quantity += quantity;
            setCartItems(updatedCartItems);
        }
    };

    const handleRemoveFromCart = (item) => {
        const updatedCartItems = cartItems.filter((cartItem) => cartItem !== item);
        setCartItems(updatedCartItems);
    };

    return (
        <div>
            <Product name="Product 1" price={9.99} onAddToCart={handleAddToCart} />
            <Product name="Product 2" price={14.99} onAddToCart={handleAddToCart} />
            <Cart items={cartItems} onRemoveFromCart={handleRemoveFromCart} />
        </div>
    );
}

export default Cart;
