import { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Container, Col } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Register() {
    const { user } = useContext(UserContext);
    const navigate = useNavigate();

    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');

    const [isActive, setIsActive] = useState(false);

    function registerUser(e) {
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email: email,
            }),
        })
            .then((res) => res.json())
            .then((data) => {
                console.log(data);

                if (data === true) {
                    Swal.fire({
                        title: 'Duplicate email found',
                        icon: 'error',
                        text: 'Kindly provide another email to complete the registration.',
                    });
                } else {
                    fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify({
                            firstName: firstName,
                            lastName: lastName,
                            email: email,
                            mobileNo: mobileNo,
                            password: password1,
                        }),
                    })
                        .then((res) => res.json())
                        .then((data) => {
                            if (data === true) {
                                setFirstName('');
                                setLastName('');
                                setEmail('');
                                setMobileNo('');
                                setPassword1('');
                                setPassword2('');

                                Swal.fire({
                                    title: 'Registration successful',
                                    icon: 'success',
                                    text: 'Welcome to Zuitt!',
                                });

                                navigate('/login');
                            } else {
                                Swal.fire({
                                    title: 'Something wrong',
                                    icon: 'error',
                                    text: 'Please try again.',
                                });
                            }
                        });
                }
            });
    }

    useEffect(() => {
        if (
            firstName !== '' &&
            lastName !== '' &&
            email !== '' &&
            mobileNo.length === 11 &&
            password1 !== '' &&
            password2 !== '' &&
            password1 === password2
        ) {
            setIsActive(true);
        } else {
            setIsActive(false);
        }
    }, [firstName, lastName, email, mobileNo, password1, password2]);

    return user.id !== null ? (
        <Navigate to="/login" />
    ) : (
        <Container fluid style={{ position: 'relative' }}>
            <video
                autoPlay
                loop
                muted
                style={{ width: '100%', height: '100%', objectFit: 'cover', opacity: 0.2 }}
            >
                <source
                    src="https://res.cloudinary.com/dv3x7qoak/video/upload/v1679817639/login_kdzlfl.mp4"
                    type="video/mp4"
                />
            </video>

            <Row
                className="justify-content-md-center"
                style={{ position: 'absolute', top: 0, left: 0, right: 0, bottom: 0 }}
            >
                <Col md={5} lg={5}>
                    <Form
                        onSubmit={(e) => registerUser(e)}
                        style={{
                            border: '1px solid #ccc',
                            padding: '20px',
                            boxShadow: '0px 0px 10px rgba(0, 0, 0, 0.2)',
                        }}
                        className="rounded-4 mt-5 mb-5 px-5  pb-5 "
                    >
                        <Col md={12} lg={12}>
                            <h3 className="text-center mb-3">
                                <img
                                    src="https://res.cloudinary.com/dv3x7qoak/image/upload/v1679811880/carrot_dvb09c.png"
                                    className="img-fluid my-md-3"
                                    style={{
                                        maxWidth: '70px',
                                        maxHeight: '70px',
                                    }}
                                />
                                Sign Up
                            </h3>
                            <h4 className="mb-4">Revolutionize your online shopping </h4>
                            <Form.Group controlId="firstName" className="mb-2">
                                {/* <Form.Label>First Name</Form.Label> */}
                                <Form.Control
                                    type="text"
                                    placeholder="First Name"
                                    value={firstName}
                                    onChange={(e) => setFirstName(e.target.value)}
                                    required
                                />
                            </Form.Group>

                            <Form.Group controlId="lastName" className="mb-2">
                                {/* <Form.Label>Last Name</Form.Label> */}
                                <Form.Control
                                    type="text"
                                    placeholder="Last Name"
                                    value={lastName}
                                    onChange={(e) => setLastName(e.target.value)}
                                    required
                                />
                            </Form.Group>

                            <Form.Group controlId="userEmail" className="mb-2">
                                {/* <Form.Label>Email address</Form.Label> */}
                                <Form.Control
                                    type="email"
                                    placeholder="Email"
                                    value={email}
                                    onChange={(e) => setEmail(e.target.value)}
                                    required
                                />
                                <Form.Text className="text-muted">
                                    We'll never share your email with anyone else.
                                </Form.Text>
                            </Form.Group>

                            <Form.Group controlId="mobileNo" className="mb-2">
                                {/* <Form.Label>Mobile Number</Form.Label> */}
                                <Form.Control
                                    type="text"
                                    placeholder="Mobile Number"
                                    value={mobileNo}
                                    onChange={(e) => setMobileNo(e.target.value)}
                                    required
                                />
                            </Form.Group>

                            <Form.Group controlId="password1" className="mb-2">
                                {/* <Form.Label>Password</Form.Label> */}
                                <Form.Control
                                    type="password"
                                    placeholder="Password"
                                    value={password1}
                                    onChange={(e) => setPassword1(e.target.value)}
                                    required
                                />
                            </Form.Group>

                            <Form.Group controlId="password2" className="mb-2">
                                {/* <Form.Label>Verify Password</Form.Label> */}
                                <Form.Control
                                    type="password"
                                    placeholder="Verify Password"
                                    value={password2}
                                    onChange={(e) => setPassword2(e.target.value)}
                                    required
                                />
                            </Form.Group>
                        </Col>
                        <Col md={12} lg={12}>
                            <Button
                                variant="primary"
                                type="submit"
                                id="submitBtn"
                                disabled={!isActive}
                                className="mt-4 mb-2 w-100 "
                            >
                                Submit
                            </Button>
                        </Col>
                    </Form>
                </Col>
            </Row>
        </Container>
    );
}
