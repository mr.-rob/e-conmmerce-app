//THIS IS ALSO THE DEFAULT PAGE RIGHT AFTER THE CUSTOMER'S LOGGED IN
import { Row, Container, Col } from 'react-bootstrap';
import { useEffect, useState } from 'react';
import CxProductCard from '../components/CxProductCard';
import Banners from '../components/Banners';

export default function CxProducts() {
    const [product, setProducts] = useState([]);

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/active`)
            .then((res) => res.json())
            .then((data) => {
                console.log(data);

                setProducts(
                    data.map((product) => {
                        return <CxProductCard key={product._id} product={product} />;
                    })
                );
            });
    }, []);

    return (
        <>
            <Container fluid>
                <Row className="mb-sm-5">
                    <Col>
                        <div
                            style={{
                                backgroundColor: '#E55C0D',
                                color: '#FFA500',
                                paddingTop: '2rem',
                                paddingBottom: '2rem',
                            }}
                        />
                    </Col>
                </Row>
            </Container>
            <Container className="mb-sm-5 mt-3 w-100">
                <Banners />
                <Row>{product}</Row>
            </Container>
        </>
    );
}
