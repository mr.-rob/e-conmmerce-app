import { useEffect, useState } from 'react';
import { Container, Row } from 'react-bootstrap';
import ProductCard from '../components/ProductCard';
import GetAll from '../components/GetAll';

export default function Products() {
    const [product, setProducts] = useState([]);

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/all`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`,
            },
        })
            .then((res) => res.json())
            .then((data) => {
                console.log(data);

                setProducts(
                    data.map((product) => {
                        return <ProductCard key={product._id} product={product} />;
                    })
                );
            });
    }, []);

    return (
        <>
            <GetAll />
            <Container>
                <Row>{product}</Row>
            </Container>
        </>
    );
}
