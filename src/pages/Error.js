import { Container, Row, Col, Button } from 'react-bootstrap';
import Badge from 'react-bootstrap/Badge';
import { Link } from 'react-router-dom';

export default function Error() {
    const data = {
        title: 'SORRY...',
        content1: 'We cannot find the page you are looking for',
        content2: 'ERROR CODE:404',
        destination: '/cxProduct',
        label: 'Back home',
    };

    return (
        <Container className="m-md-5 m-sm-0 ">
            <Row className="justify-content-center  align-items-center">
                <Col md={6}>
                    <img
                        src="https://d1rlzxa98cyc61.cloudfront.net/wysiwyg/pages/static/404.png"
                        className="img-fluid"
                    ></img>
                </Col>
                <Col md={4}>
                    <h1 className="text-danger">{data.title}</h1>
                    <h3>{data.content1}</h3>
                    <h4>
                        <Badge bg="secondary">{data.content2}</Badge>
                    </h4>

                    <h1>
                        <Button as={Link} to={data.destination} className="text-light">
                            {data.label}
                        </Button>
                    </h1>
                </Col>
            </Row>
        </Container>
    );
}
