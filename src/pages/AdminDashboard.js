import Banner from '../components/Banner';
import { Container } from 'react-bootstrap';
import AdminOptions from '../components/AdminOptions';

export default function Home() {
    const data = {
        title: 'COEX Mall',
        content: 'Shop now, or else I will buy you!',
        destination: '/products',
        label: 'Get discounts!',
    };

    return (
        <>
            <Container fluid>
                <AdminOptions />
            </Container>
        </>
    );
}
