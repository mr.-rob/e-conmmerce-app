import React, { useState } from 'react';
import { Container, Row, Col, Form, Button } from 'react-bootstrap';

function Product({ name, price, onAddToCart }) {
    const [quantity, setQuantity] = useState(1);

    const handleQuantityChange = (event) => {
        setQuantity(parseInt(event.target.value));
    };

    const handleAddToCartClick = () => {
        onAddToCart(name, price, quantity);
    };

    return (
        <div>
            <h3>{name}</h3>
            <p>${price.toFixed(2)}</p>
            <Form.Group>
                <Form.Label htmlFor="quantity">Quantity:</Form.Label>
                <Form.Control
                    type="number"
                    id="quantity"
                    name="quantity"
                    min="1"
                    value={quantity}
                    onChange={handleQuantityChange}
                />
            </Form.Group>
            <Button onClick={handleAddToCartClick} variant="primary">
                Add to Cart
            </Button>
        </div>
    );
}

function Cart({ items, onRemoveFromCart }) {
    const getTotalPrice = () => {
        return items.reduce((total, item) => total + item.price * item.quantity, 0);
    };

    const handleRemoveFromCartClick = (item) => {
        onRemoveFromCart(item);
    };

    return (
        <div>
            <h3>Shopping Cart</h3>
            {items.map((item, index) => (
                <div key={index}>
                    <p>
                        {item.name} ({item.quantity}) - ${item.price.toFixed(2)} each
                    </p>
                    <Button onClick={() => handleRemoveFromCartClick(item)} variant="danger">
                        Remove
                    </Button>
                </div>
            ))}
            <p>Total: ${getTotalPrice().toFixed(2)}</p>
        </div>
    );
}

function AddToCart() {
    const [cartItems, setCartItems] = useState([]);

    const handleAddToCart = (name, price, quantity) => {
        const itemIndex = cartItems.findIndex((item) => item.name === name);
        if (itemIndex === -1) {
            // Item is not yet in the cart
            setCartItems([...cartItems, { name, price, quantity }]);
        } else {
            // Item is already in the cart
            const updatedCartItems = [...cartItems];
            updatedCartItems[itemIndex].quantity += quantity;
            setCartItems(updatedCartItems);
        }
    };

    const handleRemoveFromCart = (item) => {
        const updatedCartItems = cartItems.filter((cartItem) => cartItem !== item);
        setCartItems(updatedCartItems);
    };

    return (
        <Container fluid>
            <Row className="justify-content-center">
                <Col md={6}>
                    <h1>Shopping</h1>
                    <Product name="Product 1" price={9.99} onAddToCart={handleAddToCart} />
                    <Product name="Product 2" price={14.99} onAddToCart={handleAddToCart} />
                </Col>
                <Col md={6}>
                    <Cart items={cartItems} onRemoveFromCart={handleRemoveFromCart} />
                </Col>
            </Row>
        </Container>
    );
}

export default AddToCart;
