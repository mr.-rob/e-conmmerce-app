import { useEffect, useState } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import CxProductCard from '../components/CxProductCard';

export default function CxProducts() {
    const [product, setProducts] = useState([]);

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/active`)
            .then((res) => res.json())
            .then((data) => {
                console.log(data);

                setProducts(
                    data.map((product) => {
                        return <CxProductCard key={product._id} product={product} />;
                    })
                );
            });
    }, []);

    return (
        <>
            <Container fluid>
                <Row className="mb-sm-3">
                    <Col>
                        <div
                            style={{
                                backgroundColor: '#E55C0D',
                                color: '#FFA500',
                                paddingTop: '2rem',
                                paddingBottom: '2rem',
                            }}
                        />
                    </Col>
                </Row>
            </Container>
            <Container>
                <Row>{product}</Row>
            </Container>
            <Container fluid>
                <Row className="">
                    <Col>
                        <div
                            style={{
                                backgroundColor: '#E55C0D',
                                color: '#FFA500',
                                paddingTop: '2rem',
                                paddingBottom: '2rem',
                            }}
                        />
                    </Col>
                </Row>
            </Container>
        </>
    );
}
